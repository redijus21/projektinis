!(function () {
  // String, Numbers, Boolean, null, undefined
  const name = "Redijus";
  const age = 39;

  // Concatenation
  console.log("My name is " + name + " and I am " + age);

  const hello = `My name is ${name} and I am ${age}`;

  const s = "technolog, computers, it, code";

  console.log(s.split(", "));
})();
