!(function () {
  //Single element
  console.log(document.getElementById("my-form"));
  console.log(document.querySelector("#my-form h1"));

  //Multiple element
  console.log(document.querySelectorAll(".item"));
  // console.log(document.getElementsByClassName('item'));
  // console.log(document.getElementsByTagName('li'));
})();
