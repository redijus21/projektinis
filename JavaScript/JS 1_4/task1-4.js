!(function () {
  const person = {
    firstName: "Red",
    lastName: "Anik",
    age: 39,
    hobbies: ["fotografija", "filmai", "sportas"],
    address: {
      street: "Svajonių 7",
      city: "Vilnius",
      country: "Lietuva",
    },
  };

  person.email = "red@example.com";
  console.log(person);
})();
