!(function () {
  // Constructor Function
  function Person(firstName, lastName, dob) {
    // Set object properties
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob); // Set to actual date object using Date constructor
    // this.getBirthYear = function(){
    //   return this.dob.getFullYear();
    // }
    // this.getFullName = function() {
    //   return `${this.firstName} ${this.lastName}`
    // }
  }
  // Get Birth Year
  Person.prototype.getBirthYear = function () {
    return this.dob.getFullYear();
  };

  // Get Full Name
  Person.prototype.getFullName = function () {
    return `${this.firstName} ${this.lastName}`;
  };

  // Instantiate an object from the class
  const person1 = new Person("Red", "Nick", "7-8-1980");
  const person2 = new Person("John", "Dove", "8-2-2020");

  console.log(person2);

  // console.log(person1.getBirthYear());
  // console.log(person1.getFullName());
})();