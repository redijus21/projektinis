!(function () {
  // Arrays - variablers that hold multiple values
  const vegetables = ["cucumber", "potato", "garlic"];

  vegetables.push("onion");

  vegetables.unshift("tomato");

  console.log(vegetables);
})();
