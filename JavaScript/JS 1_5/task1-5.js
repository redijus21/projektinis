!(function () {
  const todos = [
    {
      id: 1,
      text: "Išgręžti polius",
      isCompleted: false,
    },
    {
      id: 2,
      text: "Genėti medžius",
      isCompleted: true,
    },
    {
      id: 3,
      text: "Sugrėbti lapus",
      isCompleted: false,
    },
    {
      id: 4,
      text: "Uždengti šiltnamį",
      isCompleted: true,
    },
    {
      id: 5,
      text: "Pasodinti vaiskrūmius",
      isCompleted: true,
    },
  ];

  // For
  for (let i = 0; i < todos.length; i++) {
    console.log(todos[i].text);
  }

  // for(let todo of todos) {
  //     console.log(todo.text);
  // }
  // While
  let i = 0;
  while (i < 6) {
    console.log(`While Loop Number: ${i}`);
    i++;
  }

  // forEach, map, filter
  todos.forEach(function (todo) {
    console.log(todo.text);
  });

  const todoText = todos.map(function (todo) {
    return todo.text;
  });

  console.log(todoText);

  const todoUncompleted = todos
    .filter(function (todo) {
      return todo.isCompleted === true;
    })
    .map(function (todo) {
      return todo.text;
    });

  console.log(todoUncompleted);
})();
