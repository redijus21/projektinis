!(function () {
  // String, Numbers, Boolean, null, undefined
  const name = "Redijus";
  const age = 39;

  // Concatenation
  console.log("Labas rytas, mano vardas " + name + ", man " + age + ".");

  const hello = `Mano vardas ${name} man ${age}`;
})();
