!(function () {
  const a = 5;
  const b = 22;

  // Abi reikšmės
  if (a > 6 && b > 20) {
    console.log("a is more than 6 or b is more than 20");
  }

  // Bent viena reikšmė
  if (a > 6 || b > 15) {
    console.log("a is more than 6 or b is more than 20");
  }
})();
